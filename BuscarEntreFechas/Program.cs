﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuscarEntreFechas
{
    class Program
    {
        static void Main(string[] args)
        {
            var listaFechas = new List<DateTime>
                    {
                        new DateTime(2018,3,5),
                        new DateTime(2018,3,6),
                        new DateTime(2018,3,7),
                        new DateTime(2018,3,9),
                        new DateTime(2018,3,10),
                        new DateTime(2018,3,11),

                        new DateTime(2018,3,12),
                        new DateTime(2018,3,13),
                        new DateTime(2018,3,14),
                        new DateTime(2018,3,17),
                        //new DateTime(2018,3,18),

                        new DateTime(2018,4,30),
                        new DateTime(2018,5,1),
                        new DateTime(2018,5,2),
                        new DateTime(2018,5,4),
                        new DateTime(2018,5,5),
                        new DateTime(2018,5,6),

                    };

            FAsistidasFaltantes(listaFechas);

            Console.Read();
        }





        private static (List<DateTime> fechasFaltantes, List<DateTime> fechasAsistidas) FAsistidasFaltantes(List<DateTime> fechas)
        {
            var fechastemp = fechas;
            var fechasFaltantes = new List<DateTime>();
            var fechasAsistidas = new List<DateTime>();
            while (fechastemp.Count != 0)
            {
                var fecha = FechaInicialYSemana(fechastemp);

                var fInicial = fecha.fechaInicio;
                var lista = fecha.Semana;
                //for (var i = 0; i <= fecha.numeroDias; i++)
                //{
                //    lista.Add(fInicial.AddDays(i));
                //}

                #region Verificando fechas faltantes 
                foreach (var item in lista)
                {
                    //para capturar fechas faltantes
                    if (!fechastemp.Contains(item))
                    {

                        if (item.DayOfWeek.ToString() == "Sunday")
                        {
                            Console.WriteLine("Es normal Faltar domingos");
                        }
                        else
                        {
                            Console.WriteLine("Fecha que falta");
                            Console.WriteLine($"{item} -------  {item.DayOfWeek}");
                        }
                        fechasFaltantes.Add(item);
                    }
                    //fechas que si vinieron
                    else
                    {
                        fechasAsistidas.Add(item);
                        if (item.DayOfWeek.ToString() == "Sunday")
                        {
                            Console.WriteLine("Vino Domingo");
                            //Console.WriteLine($"{item} -------  {item.DayOfWeek}");
                        }
                        fechastemp.Remove(item);
                    }
                }
                #endregion
            }

            #region Prueba por si hay mas
            //Console.WriteLine("Veamos si hay mas");
            //foreach (var item in fechastemp)
            //{
            //    Console.WriteLine("------------");
            //    Console.WriteLine(item);
            //}
            #endregion

            return (fechasFaltantes, fechasAsistidas);
        }

        //Usando Tuplas
        /// <summary>
        /// Retorna la fecha inicial y la semana del menor valor
        /// </summary>
        /// <param name="fechas"></param>
        /// <returns></returns>
        private static (DateTime fechaInicio, List<DateTime> Semana) FechaInicialYSemana(List<DateTime> fechas)
        {
            DateTime fechaInicio = new DateTime();
            List<DateTime> semana = new List<DateTime>();
            switch (fechas.Min().DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    fechaInicio = fechas.Min().AddDays(-6);
                    break;
                case DayOfWeek.Monday:
                    fechaInicio = fechas.Min();      
                    break;
                case DayOfWeek.Tuesday:
                    fechaInicio = fechas.Min().AddDays(-1);
                    break;
                case DayOfWeek.Wednesday:
                    fechaInicio = fechas.Min().AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    fechaInicio = fechas.Min().AddDays(-3);
                    break;
                case DayOfWeek.Friday:
                    fechaInicio = fechas.Min().AddDays(-4);
                    break;
                case DayOfWeek.Saturday:
                    fechaInicio = fechas.Min().AddDays(-5);
                    break;
            }

            for (var i = 0; i <= 6; i++)
            {
                semana.Add(fechaInicio.AddDays(i));
            }


            return (fechaInicio,semana);
        }
    }
}
